#include<iostream>
#include<ctype.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
int main()
{
  char ch;
  std::string msg;

  std::cout<<"Enter a character: ";
  std::cin>>ch;
  if(isalpha(ch))
    {
      if((ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') || (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'))
	msg = "Is vowel";
      else
	msg = "Is consonant";
    }
  else
    {
      if(isdigit(ch))
	msg = "Is digit";
      else if(ispunct(ch))
	msg = "Is punctuation";
      else
	msg = "Is unrecognised";
    }
  std::cout<<msg;
}
	
